<?php
// @todo warmup cache command

namespace Innomedio\ImageCacheBundle\Service;

use Grafika\Grafika;
use Monolog\Logger;
use Symfony\Component\Filesystem\Filesystem;

class ImageHandler
{
    private $fs;
    private $logger;
    private $projectDir;

    private $image;
    private $width = 400;
    private $height = 400;
    private $position = "smart";
    private $type = "crop";
    private $quality = 100;

    public function __construct(Filesystem $fs, Logger $logger, $projectDir)
    {
        $this->fs = $fs;
        $this->logger = $logger;
        $this->projectDir = $projectDir;
    }

    /**
     * @param $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @param string $position
     */
    public function setPosition(string $position): void
    {
        $this->position = $position;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @param $width
     * @param $height
     */
    public function setDimensions($width, $height)
    {
        $this->width = $width;
        $this->height = $height;
    }

    /**
     * @return bool|string
     */
    public function resizeImage()
    {
        $dirExplode = explode("/", $this->image);
        end($dirExplode);
        $filename = $dirExplode[key($dirExplode)];

        $originFile = $this->projectDir . $this->image;
        $websiteDir = "/cache" . str_replace($filename, '', $this->image) . $filename . "/";
        $outputDir = $this->projectDir . "/public/" . $websiteDir;
        $outputFilename = $this->width . "x" . $this->height . "-" . $this->position . "-" . $this->type . "-" . $this->quality . "." . pathinfo($filename, PATHINFO_EXTENSION);

        if (!$this->fs->exists($outputDir . $outputFilename)) {
            if ($this->width === 0 || $this->height === 0) {
                $this->fs->copy($originFile, $outputDir . $outputFilename);
            } else {
                try {
                    $handle = Grafika::createEditor();
                    $handle->open($image, $originFile);

                    switch ($this->type) {
                        case "crop":
                            $handle->crop($image, $this->width, $this->height, $this->position);
                            break;

                        case "fit":
                            $handle->resizeFit($image, $this->width, $this->height);
                            break;

                        case "fill":
                            $handle->resizeFill($image, $this->width, $this->height);
                            break;

                        default:
                            throw new \Exception("Invalid type given: " . $this->type, null, $this->quality);
                            break;
                    }

                    $handle->save($image, $outputDir . $outputFilename);
                } catch (\Exception $e) {
                    $this->logger->addError($e->getMessage());
                    return false;
                }
            }
        }

        return $websiteDir . $outputFilename;
    }

    /**
     * @return bool
     */
    public function removeCachedImage()
    {
        if (!empty($this->image)) {
            $imageDir = $this->projectDir . "/public/cache" . $this->image . "/";
            $this->fs->remove($imageDir);
            return true;
        }

        return false;
    }
}