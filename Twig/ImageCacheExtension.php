<?php
namespace Innomedio\ImageCacheBundle\Twig;

use Innomedio\ImageCacheBundle\Service\ImageHandler;
use Twig\TwigFunction;

class ImageCacheExtension extends \Twig_Extension
{
    private $handler;

    /**
     * ImageCacheExtension constructor.
     * @param ImageHandler $handler
     */
    public function __construct(ImageHandler $handler)
    {
        $this->handler = $handler;
    }

    /**
     * @return array|\Twig_Function[]
     */
    public function getFunctions()
    {
        return array(
            new TwigFunction('cache', array($this, 'cache'))
        );
    }

    /**
     * @param $file
     * @param $height
     * @param $width
     * @param string $type
     * @param string $position
     * @return bool|string
     */
    public function cache($file, $height, $width, $type = 'fill', $position = 'smart')
    {
        $this->handler->setImage($file);
        $this->handler->setDimensions($height, $width);
        $this->handler->setType($type);
        $this->handler->setPosition($position);

        return $this->handler->resizeImage();
    }
}