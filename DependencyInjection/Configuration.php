<?php
namespace Innomedio\ImageCacheBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('innomedio_image_cache');


        $rootNode
            ->children()
                ->scalarNode('cache_dir')
                    ->defaultValue('cache')
                ->end()
                ->scalarNode('public_dir')
                    ->defaultValue('public')
                ->end()
            ->end();

        return $treeBuilder;
    }
}
